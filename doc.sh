# -*- Mode:Shell-script -*-
ocamlbuild -docflags '-I bytecomp, -keep-code' typing.docdir/index.html
ocamlbuild -docflags '-I bytecomp' typing.docdir/index.dot
# ocamlbuild -docflags '-keep-code' parsing.docdir/index.html

ocamlbuild -docflags '-I toplevel, -keep-code' camlp4.docdir/index.html
